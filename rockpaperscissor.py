import getpass

print("Rock...")
print("Paper...")
print("Scissor...")

player1 = getpass.getpass(prompt = "Player 1, make your move: ")
player2 = getpass.getpass(prompt = "Player 2, make your move: ")

if player1 != player2:
	if player1 == "rock" and player2 == "scissor":
		print("Player 1 wins!")
	elif player1 == "rock" and player2 == "paper":
		print("Player 1 wins!")
	elif player1 == "paper" and player2 == "scissor":
		print("Player 2 wins!")
	elif player1 == "paper" and player2 == "rock":
		print("Player 1 wints!")
	elif player1 == "scissor" and player2 == "rock":
		print("Player 2 wins!")
	elif player1 == "scissor" and player2 == "paper":
		print("Player 1 wins!")
else:
	print("TIE! PLAY AGAIN!")
